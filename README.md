# Challenge

The Challenge Build a simple Appointment web application between doctors and patients using nodejs, python or golang for the backend.
Patient should be available to view all the available time slots  of a doctor so he would be able to make an appointment. more at [CHALLENGE](./CHALLENGE.md)

------

## Status

Under development and not finished task

------

## Install

For full [installation docs](./docs/install.rst) follow the link.

for fast up and running:

```bash
    docker-compose up
```

------

## Tasks

- [ ] Build a doctors service
- [x] Use docker
- [x] Use docker-compose
- [x] Use git flow
- [x] Add README.md
- [ ] Use Kubernets
- [ ] Build patient service
- [ ] Mulitple databases
- [ ] Configure editor configuration
- [ ] Code Documentation
- [ ] Code Coverage
- [x] Following TDD

------
