# Backend development challenge

Hello, as a seasoned engineer prospect we expect you to have more on the ground experience & knowledge than other fellow members. The proposed task is designed to measure various aspects of your skills such as problem solving, code quality and architectural approach. This is an opportunity to elaborate your engineering mindset and your technical skills. Take your time to finish the assignment but be careful not to invest too much time on it (we recommend to use 16h maximum for this assignment), because our goal is not to make you work spend countless hours on a non paid assignment but how you deal with software architecture and design. We are interested in your process and your problem solving, why you made the decisions you made.
The Challenge Build a simple Appointment web application between doctors and patients using nodejs, python or golang for the backend.
Patient should be available to view all the available time slots  of a doctor so he would be able to make an appointment.

## Application Requirements

- Simple registration for doctors and patients.
- Patients can make appointments with available doctors.
- Decoupling different application services in a microservices architecture (i.e doctors, patients and appointments).

## Rules & Hints

- Consider database design; carefully create your tables and indexes. (Local or Cloud Database Services i.e firebase )
- You are free to code your solution using libraries and the database of your choice. Sharing your reasoning behind choosing them is welcome!
- Ensure the flow of your application is as efficient and performant as possible. Hint: Use caching, messaging and queueing, …etc. (If required)
- Push your code to Github with descriptive commit messages along with a README to deploy and test.
- A proper argumentation of the code structure and the following design pattern Utilize Google Cloud Platform tools (there’s a free trial to play with)

## The Task will be evaluated for

- System Architecture.
- Code architecture.
- Code maintainability.
- Code reusability.
- Code modularity.
- Code quality.
- Code coverage
- Code Documentation
- Using AMQP (e.g. Google Pub/Sub)  if needed
- Using containerized environments (Docker , Docker-Compose, K8s).
- Different programming languages for each service.
- Different Databases for each service .
- Endpoints Authentication.

## Delivery

- Please submit your task over a publicly accessible git Repo along with any documentation to test/run/deploy
- Using Firebase or Google Cloud Platform is a PLUS
- Live deployment over services such as Heroku or a cloud provider is a PLUS
- Implementation using methodologies such as TDD is a PLUS
- Attaching cover coverage and Quality reports is a PLUS
