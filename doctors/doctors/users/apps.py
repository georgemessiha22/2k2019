from django.apps import AppConfig


class UsersAppConfig(AppConfig):

    name = "doctors.users"
    verbose_name = "Users"
