from django.contrib.auth.models import AbstractUser
from django.db.models import CharField, SmallIntegerField
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class User(AbstractUser):
    user_types = (
        (0, 'Doctor'),
        (1, 'Patient')
    )
    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = CharField(_("Name of User"), blank=True, max_length=255)
    type = SmallIntegerField(choices=user_types, blank=False, null=False)

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"username": self.username})
