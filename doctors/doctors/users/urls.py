from django.urls import path

from doctors.users.views import (
    user_list_view,
    user_redirect_view,
    user_update_view,
    user_detail_view,
    UserViewSet,
)

app_name = "users"
urlpatterns = [
    path("", view=user_list_view, name="list"),
    path("~redirect/", view=user_redirect_view, name="redirect"),
    path("~update/", view=user_update_view, name="update"),
    path("<str:username>/", view=user_detail_view, name="detail"),
]

from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(prefix='user-api', viewset=UserViewSet)
urlpatterns += router.urls
