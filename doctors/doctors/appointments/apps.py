from django.apps import AppConfig


class AppointmentsAppConfig(AppConfig):

    name = 'doctors.appointments'
    verbose_name = 'Appointments'
